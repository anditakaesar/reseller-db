import { ENV_STR } from './str'

const NODE_ENV = process.env.NODE_ENV || ENV_STR.PROD
const PORT = parseInt(process.env.PORT, 10) || 3000

const env = {
  NODE_ENV, PORT
}

export default env