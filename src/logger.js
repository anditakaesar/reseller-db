import winston, { format } from 'winston'
import moment from 'moment'
import path from 'path'
import { ENV_STR } from './str'
import env from './env'

const {
  combine, timestamp, label, printf,
} = format
const myFormat = printf((info) => {
  const metas = []
  Object.keys(info).forEach((e) => {
    let value = ''
    if (e !== 'timestamp' && e !== 'label' && e !== 'level' && e !== 'message') {
      value = info[e]
      if (value !== '' && value !== undefined) {
        metas.push(`${e}:${value}`)
      }
    }
  })

  return `${info.timestamp} [${info.label}] ${info.level.toUpperCase()} ${info.message} | ${metas.join(' | ')}`
})

const dateStr = moment().format('YYYY-MM-DD')
const FILE_CONFIG = {
  DEV: {
    filename: path.join(__dirname, `../logs/${dateStr}.log`),
    level: 'debug',
    format: combine(
      label({ label: 'DEVELOP' }),
      timestamp(),
      myFormat,
    ),
  },
}

const CONSOLE_CONFIG = {
  DEV: {
    level: 'debug',
    format: combine(
      label({ label: 'DEVELOP' }),
      timestamp(),
      myFormat,
    ),
  },
}

function InitLogger() {
  if (env.NODE_ENV === ENV_STR.DEV) {
    return new winston.createLogger({
      transports: [
        new winston.transports.File(FILE_CONFIG.DEV),
        new winston.transports.Console(CONSOLE_CONFIG.DEV),
      ],
    }) // end return
  }

  return new winston.createLogger({
    transports: [
      new winston.transports.File(fileConfig),
    ],
  })
}

const logger = InitLogger()

export default logger