import express from 'express'

const app = express()

app.use((req, res) => {
  res.status(200).json({
    message: 'first message'
  })
})

export const logger = require('./logger').default
export default app