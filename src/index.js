import app, { logger } from './app'
import env from './env'
import { ENV_STR } from './str'

app.listen(env.PORT, () => {
  logger.info('app running', { port: env.PORT })
  if (env.NODE_ENV === ENV_STR.DEV) {
    console.log('env', env)
  }
})